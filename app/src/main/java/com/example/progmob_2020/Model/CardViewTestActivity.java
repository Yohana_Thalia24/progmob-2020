package com.example.progmob_2020.Model;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;


import com.example.progmob_2020.Adapter.MahasiswaCardAdapter;
import com.example.progmob_2020.R;

import java.util.ArrayList;
import java.util.List;

public class CardViewTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view_test);

        RecyclerView cv = (RecyclerView)findViewById(R.id.rvCard);
        MahasiswaCardAdapter mahasiswaCardAdapter;

        //data dummy
        List<Mahasiswa> mahasiswaList = new ArrayList<Mahasiswa>();

        //generate data mahasiswa
        Mahasiswa m1 = new Mahasiswa("Yohana Thalia", "72170144", "0811111111");
        Mahasiswa m2 = new Mahasiswa("Aimiya", "72170142", "082222222");
        Mahasiswa m3 = new Mahasiswa("Skolastika", "72170136", "083333333");
        Mahasiswa m4 = new Mahasiswa("Wulan", "72170000", "08444444");
        Mahasiswa m5 = new Mahasiswa("Bramastya", "72150020", "08555555");
       


        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);
        

        mahasiswaCardAdapter= new MahasiswaCardAdapter(CardViewTestActivity.this);
        mahasiswaCardAdapter.setMahasiswaList(mahasiswaList);

        cv.setLayoutManager(new LinearLayoutManager(CardViewTestActivity.this));
        cv.setAdapter(mahasiswaCardAdapter);
    }
}