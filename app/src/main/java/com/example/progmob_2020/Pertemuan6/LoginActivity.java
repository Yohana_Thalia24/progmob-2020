package com.example.progmob_2020.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.progmob_2020.Model.User;
import com.example.progmob_2020.Network.GetDataService;
import com.example.progmob_2020.Network.RetrofitClientInstance;
import com.example.progmob_2020.Pertemuan6.MainMenuActivity;
import com.example.progmob_2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    ProgressDialog pd;
    String isLogin="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText nimnik =(EditText)findViewById(R.id.nimnik);
        final EditText password =(EditText)findViewById(R.id.password);
        Button btnLogin =(Button)findViewById(R.id.btnlogin);
        pd=new ProgressDialog(LoginActivity.this);

        SharedPreferences pref = LoginActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        isLogin =pref.getString("isLogin","0");
        if(isLogin.equals("1")) {
            Intent intent = new Intent(LoginActivity.this, MainMenuActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Log In");
                pd.show();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<User>> login = service.login(
                        nimnik.getText().toString(),
                        password.getText().toString()
                );


                login.enqueue(new Callback<List<User>>() {

                    @Override
                    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                        if (response.body().size() != 0) {
                            editor.putString("isLogin", "1");
                            editor.commit();
                            pd.dismiss();
                            Intent intent = new Intent(LoginActivity.this, MainMenuActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            Toast.makeText(LoginActivity.this, "success", Toast.LENGTH_LONG).show();
                        } else {
                            pd.dismiss();
                            Toast.makeText(LoginActivity.this, "failed", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<User>> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(LoginActivity.this,"Failed",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }
}